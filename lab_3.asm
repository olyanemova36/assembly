  .arch  armv8-a
  .data
  .align 3
usage:
  .string "Usage: %s file\n"
i:
  .string "Coordinate of i: %f\n"
j:
  .string "coordinate of j: %f\n"
k:
  .string "Coordinate of k: %f\n"
format:
  .string "Incorrect format of file\n"
size:
  .string "Number of vectors must be less or equal 20\n"
formint:
  .string "%d"
formdouble:
  .string "%lf"
mode:
  .string "r"
indexs:
  .quad 1, 5, 2, 4, 0, 5, 2, 3, 0, 4, 1, 3
  .text
  .align 2
  .global main
  .type main, %function
  .equ progname, 32
  .equ filename, 40
  .equ filestruct, 48
  .equ n,56
  .equ matrix, 64
  .equ icor, 544
  .equ jcor, 552
  .equ kcor, 560
main:
  sub sp, sp, #568
  stp x29, x30, [sp]
  stp x27, x28, [sp, #16]
  mov x29, sp
  cmp w0, #2
  beq 0f
  ldr x2, [x1]
  adr x0, stderr
  ldr x0, [x0]
  adr x1, usage
  bl fprintf
9:
  mov x0, #1
  ldp x29, x30, [sp]
  ldp x27, x28, [sp, #16]
  add sp, sp, #568
  ret
0:
  ldr x0, [x1]
  str x0, [x29, progname]
  ldr x0, [x1, #8]
  str x0, [x29, filename]
  adr x1, mode
  bl fopen
  cbnz x0, 1f
  ldr x0, [x29, filename]
  bl perror
  b 9b
1:
  str  x0, [x29, filestruct]
  adr x1, formint
  add x2, x29, n
  bl fscanf
  cmp w0, #1
  beq 2f
  ldr x0, [x29, filestruct]
  bl fclose
  adr x0, stderr
  ldr x0, [x0]
  adr  x1, format
  bl printf
  b 9b
2:
  mov x27, #0
  ldr x28, [x29, n]
  cmp x28, #0
  ble 3f
  cmp x28, #20
  bgt 3f
  mov x0, #3
  mul x28, x28, x0
  b 4f
3:
  ldr x0, [x29, filestruct]
  bl fclose
  adr x0, stderr
  ldr x0, [x0]
  adr x1, size
  bl printf
  b 9b
4:
  ldr x0, [x29, filestruct]
  adr x1, formdouble
  lsl x2, x27, #3
  add x2, x2, x29
  add x2, x2, matrix
  bl fscanf
  cmp w0, #1
  beq 5f
  ldr x0, [x29, filestruct]
  bl fclose
  adr x0, stderr
  ldr x0, [x0]
  adr x1, format
  bl printf
  b 9b
5:
  add x27, x27, #1
  cmp x27, x28
  bne 4b
  ldr x0, [x29, filestruct]
  bl fclose
  ldr x28, [x29, n]
  mov x27, #3
  sub x28, x28, #1
  mov x0, #0     // - counter for elements (coordinates)
  adr x4, indexs
  mov x1, #0
  mov x8, #0
  add x9, x29, matrix
  fmov d0, xzr   // coordinate for i
  fmov d1, xzr   // coordinate for j
  fmov d2, xzr   // coordinate for k
  fmov d4, #1.0
  fmov d5, #1.0
7:
  cmp x1, x28
  beq 1f
  lsl x2, x0, #3
  mul x2, x1, x27
  lsl x2, x2, #3
  add x2, x2, x9   // Adress of begining coordinate matriv
5:
  mov x10, #0
  ldr x3, [x4, x0, lsl #3]
  ldr d3, [x2, x3, lsl #3]
  fmul d4, d4, d3
  add x0, x0, #1
  ldr x3, [x4, x0, lsl #3]
  ldr d3, [x2, x3, lsl #3]
  fmul d4, d4, d3
  add x0, x0, #1
  ldr x3, [x4, x0, lsl #3]
  ldr d3, [x2, x3, lsl #3]
  fmul d5, d5, d3
  add x0, x0, #1
  ldr x3, [x4, x0, lsl #3]
  ldr d3, [x2, x3, lsl #3]
  fmul d5, d5, d3
  cmp x0, #3
  beq 8f
  cmp x0, #7
  beq 9f
  cmp x0, #11
  beq 0f
8:
  fsub d0, d4, d5
  add x10, x29, icor
  str d0, [x10]
  fmov d4, #1.0
  fmov d5, #1.0
  add x0, x0, #1
  b 5b
9:
  fsub d1, d4, d5
  add x10, x29, jcor
  str d1, [x10]
  fmov d4, #1.0
  fmov d5, #1.0
  add x0, x0, #1
  b 5b
0:
  fsub d2, d4, d5
  add x10, x29, kcor
  str d2, [x10]
  add x1, x1, #1
  mul x27, x27, x1
  mov x8, #0
  add x8, x8, x27
  str d0, [x2, x8, lsl #3]
  add x8, x8, #1
  str d1, [x2, x8, lsl #3]
  add x8, x8, #1
  str d2, [x2, x8, lsl #3]
  mov x27, #3
  mov x0, #0
  mov x8, #0
  fmov d4, #1.0
  fmov d5, #1.0
  b 7b
1:
  adr x0, i
  bl printf
  mov w0, #0
  ldr d0, [x29, jcor]
  adr x0, j
  bl printf
  mov w0, #0
  ldr d0, [x29, kcor]
  adr x0, k
  bl printf
  mov w0, #0
  ldp x29, x30, [sp]
  ldp x27, x28, [sp, #16]
  add sp, sp, #544
  ret
  .size main, .-main
