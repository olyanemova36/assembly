    .arch armv8-a
    .data
    .align  3
  n:
    .word   3
  m:
    .word   5
  matrix:
    .quad   4, 6, 1, 8, 2
    .quad   1, 2, 3, 4, 5
    .quad   1, 9, 3, 3, 6
    .text
    .align  2
    .global _start
    .type   _start, %function
  _start:
    adr x2, n
    mov x11, #0
    mov x0, #0
    ldr w0, [x2]
    adr x2, m
    mov x1, #0
    ldr w1, [x2]
    adr x2, matrix
    mov x4, #1
  L0:
    cmp x4, x1
    sub x3, x4, #1
    lsl x6, x3, #3
    bgt L9
    mov x3, #1
  L1:
    cmp x3, x0
    bge L5
    add x6, x6, x1, lsl #3
    sub x9, x6, x1, lsl #3
    ldr x7, [x2, x6]
    sub x5, x3, #1
  L2:
    cmp x11, x5
    bgt L3
    mov x12, x9
    ldr x8, [x2, x9]
    cmp x8, x7
    bge L3
    add x9, x9, x1, lsl #3
    str x8, [x2, x9]
    sub x5, x5, #1
    sub x9, x12, x1, lsl #3
    b L2
  L3:
    add x9, x9, x1, lsl #3
    str x7, [x2, x9]
    add x3, x3, #1
    b L1
  L5:
    add x4, x4, #1
    b L0
  L9:
    mov x0, #0
    mov x8, #93
    svc #0
    .size   _start, .-_start
