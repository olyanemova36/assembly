  .arch armv8-a
  .data
result:
  .string "Your conjoined twins-words\n"
  .equ resultlen, .-result
errmes1:
  .string "Usage: "
  .equ errlen1, .-errmes1
errmes2:
  .string "file\n"
  .equ errlen2, .-errmes2
filename1:
  .skip   1024
  .align  3
fd1:
  .skip   8
  .text
  .align  2
  .global _start
  .type   _start, %function
_start:
  ldr x0, [sp]
  cmp x0, #2
  beq 2f
  mov x0, #-2
  bl _writeerr
  b 3f
  ldr x1, [sp, #8]
  mov x2, #0
0:
  ldrb    w3, [x1, x2]
  cbz w3, 1f
  add x2, x2, #1
  b   0b
1:
  mov x8, #64
  svc #0
  mov x0, #2
  adr x1, errmes2
  mov x2, errlen2
  mov x8, #64
  svc #0
  mov x0, #1
  b   3f
2:
  mov x0, #2
  adr x1, result
  mov x2, resultlen
  mov x8, #64
  svc #0
  ldr x0, [sp, #16]
  bl _work
3:
  mov x8, #93
  svc #0
  .size   _start, .-_start
  .type   _work, %function
  .data
del:
  .string " "
  .equ dellen, .-del
end:
  .string "\n"
  .equ endlen, .-end
  .text
newstr:
  .skip   1024
  .equ    filename, 16
  .equ    fd, 24
  .equ    buf, 32
_work:
  mov x16, 2088
  sub sp, sp, x16  //-2088
  stp x29, x30, [sp]
  mov x29, sp   // сохраняем в х29 указатель
  str x0, [x29, filename]
  mov x1, x0
  mov x0, #-100
  mov x2, #0
  mov x8, #56
  svc #0
  bge 0f
  bl _writeerr
  cmp x0, #0
  b 6f
0:
  str x0, [x29, fd]
1:
  ldr x0, [x29, fd]
  add x1, x29, buf
  mov x2, #2048
  mov x8, #63
  svc #0
  cmp x0, #0   // -
  beq 7f
  cmp x0, #2048
  blt 5f
  mov x3, x0
  add x3, x3, x1
3:
  ldrb w4, [x3, #-1]
  sub x0, x0, #1
  cmp w4, '\n'
  beq 6f
  cmp w4, '\0'
  beq 6f
  cmp x0, #0
  mov x0, x2
  add x1, x1, x0
  add x3, x3, x1
  beq 4f
  b 3b
4:
  ldrb w4, [x3, #-1]
  sub x3, x3, #1
  cmp w4, #32
  beq 6f
  bge 4b
6:
  sub x1, x2, x3
  ldr x0, [x29, fd]
  mov x2, #1
  mov x8, #62
  svc #0
5:
  sub x1, x1, #1
  adr x3, newstr
  mov x16, buf
  add x1, x29, x16  // х1 содержит указатель на начало buf
  mov x4, x3     //х4 = х3 содеражт указатели на начало вывод строки
2:
  ldrb w0, [x1], #1
  cbz w0, 1b
  cmp w0,' '
  beq 2b
  cmp w0, #10
  beq 2b
  cmp w0, #9
  beq 2b
  cmp x4, x3
  beq 3f
  mov w0, ' '
  strb w0, [x3], #1
3:
  sub x2, x1, #1    // Помещаем в х2 указатель на начало слова
4:
  ldrb w0, [x1], #1
  cbz w0, 1b
  cmp w0, #10
  beq 5f
  cmp w0, #9
  beq 5f
  cmp w0, ' '
  bne 4b
5:
  sub x5, x1, #1  // x5 - указатель  на конец слова х2 - на начало
  sub x7, x5, x2 //  х7 - длина слова
  mov x9, x2     // указатель на начало слова
  mov x8, x7
6:
  ldrb w0, [x5, #-1]!
  ldrb w6, [x2], #1
  cmp w0, w6
  bne 5f
  sub x8, x8, #2
  cmp x8, #0
  beq 1f
  cmp x8, #1
  beq 1f
  cmp x5, x2
  bgt 6b
7:
  mov x1, end
  mov x0, #2
  mov x2, endlen
  mov x8, #64
  svc #0
  b 8f
5:
  b 2b
1:
  mov x0, #0    // Печать слова
  mov x2, x7
  add x2, x2, #1
  mov x1, x9
  mov x8, #64
  svc #0
  mov x1, #0
  add x1, x9, x7
  b 2b
5:
  b 2b
  8:
  mov x0, #0
  mov x8, #93
  svc #0
  .size _work, .-_work
  .type _writeerr, %function
  .data
nofile:
  .string "File parameter doesn't attached to parameters\n"
  .equ nofilelen, .-nofile
permition:
  .string "Permition denied\n"
  .equ permitionlen, .-permition
unknown:
  .string "Unknown error\n"
  .equ unknownlen, .-unknown
  .align 2
  .text
_writeerr:
  cmp x0, #-2
  bne 0f
  adr x1, nofile
  mov x2, nofilelen
  b 2f
0:
  cmp x0, #-13
  bne 1f
  adr x1, permition
  mov x2, permitionlen
  b 2f
1:
  adr x1, unknown
  mov x2, unknownlen
2:
  mov x0, #2
  mov x8, #64
  svc #0
  ret
  .size _writeerr, .-_writeerr
