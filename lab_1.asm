    .arch armv8-a
  //res = (((a*e) - (b*c)+(d/b)))/((b+c)*a)
    .data
    .align  3
  res:
    .skip   8
  a:
    .word   200000
  c:
    .word   30000
  e:
    .word   100000
  b:
    .short  1000
  d:
    .short  5000
    .text
    .align  2
    .global _start
    .type   _start, %function
  _start:
    adr x0, a
    ldr w1, [x0]
    adr x0, b
    ldrsh   w2, [x0]
    adr x0, c
    ldrh    w3, [x0]
    adr x0, d
    ldrsh   w4, [x0]
    adr x0, e
    ldr w5, [x0]
    mov x13, #0
    add x6, x2, x3  //b + c
    sxtw x1, w1
    mul x7, x6, x1              //(b + c)*a
    smull x6, w1, w5            // a*e
    smull x8, w2, w3            // b*c
    sdiv    x9, x4, x2          // d/b
    adds    x10, x6, x9         //(a*e + d/b)
    subs x10, x10, x8           //(a*e - b*c + d/b)
    sdiv x8, x10, x7
    b L2
  L1:
    mov x8, #0
  L2:
    adr x0, res
    str x8, [x0]
    mov x0, #0
    mov x8, #93
    svc #0
    .size   _start, .-_start
